<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can signup API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/signup', 'UsersController@signup');
Route::post('user/login', 'UsersController@login');

Route::post('reset-password', 'ResetPasswordController@sendMail');
Route::put('reset-password', 'ResetPasswordController@reset');

Route::get('drinks/fetch-drinks', 'DrinksController@fetchDrinks');
Route::get('drinks/fetch-detail-drinks', 'DrinksController@fetchDetailDrinks');
Route::post('cart/add-to-cart', 'CartController@addToCart');
Route::get('cart/fetch-data-cart', 'CartController@fetchDataCart');
Route::post('cart/increase-item-cart', 'CartController@increaseItemCart');
Route::post('cart/decrease-item-cart', 'CartController@decreaseItemCart');
Route::post('cart/clear-cart', 'CartController@clearCart');
Route::post('cart/order', 'CartController@order');
Route::get('order/order-history', 'OrdersController@fetchOrders');
Route::get('order/filter-order-history', 'OrdersController@filterOrders');
