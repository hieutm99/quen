<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount_order', 'price_order', 'order_id', 'drinks_id'
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\Orders');
    }

    public function drinks()
    {
        return $this->hasMany('App\Models\Drinks');
    }

    public function addMoreDetailOrder($idOrder, $payload){
        foreach ($payload as $item){
            $detailOrder = new DetailOrder;
            $detailOrder->amount_order = $item->amount;
            $detailOrder->price_order = $item->price;
            $detailOrder->order_id = $idOrder;
            $detailOrder->drinks_id = $item->idDrinks;
            $detailOrder->save();
        }
    }

    public function getDataOrdersByIdOrder($idOrder)
    {
        $dataOrdersByIdOrder = DetailOrder::join('drinks', 'detail_orders.drinks_id', '=', 'drinks.id')
            ->select('drinks.name', 'drinks.image', 'detail_orders.amount_order', 'detail_orders.price_order')
            ->where('detail_orders.order_id', $idOrder)
            ->paginate(1);
        return $dataOrdersByIdOrder;
    }
}
