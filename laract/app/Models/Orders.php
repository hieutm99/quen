<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_order', 'status', 'user_id'
    ];

    public function users()
    {
        return $this->hasMany('App\Users');
    }

    public function detailOrder()
    {
        return $this->belongsTo('App\Models\DetailOrder');
    }

    public function insertOrder($idUser){
        $order = new Orders;
        $order->date_order = date("Y-m-d h:i:s");
        $order->status = 0;
        $order->user_id = $idUser;
        $order->save();
        return $order->id;
    }

    public function getDatetimeOrders($idUser)
    {
        $datetimeOrders = Orders::select('orders.id', 'orders.date_order')
            ->where('orders.user_id', $idUser)
            ->get();
        return $datetimeOrders;
    }
}
