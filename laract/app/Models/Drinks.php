<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Drinks extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type_drinks_id', 'image', 'price', 'description', 'amount'
    ];

    public function typeDrinks()
    {
        return $this->hasMany('App\Models\TypeDrinks');
    }

    public function detailOrder()
    {
        return $this->belongsTo('App\Models\DetailOrder');
    }

    public function cart()
    {
        return $this->belongsTo('App\Models\Cart');
    }

    public function getDataDrinks()
    {
        $fetchDrinks = Drinks::select('drinks.id', 'drinks.name', 'drinks.type_drinks_id as typeId', 'drinks.image', 'drinks.price')
            ->get();
        return $fetchDrinks;
    }

    public function getDetailDrinks($idDrinks) {
        $detailDrinks = Drinks::join('type_drinks', 'drinks.type_drinks_id', '=', 'type_drinks.id')
            ->select('drinks.id', 'drinks.name', 'drinks.image', 'drinks.price', 'drinks.description', 'type_drinks.name as typeName')
            ->where('drinks.id', '=', $idDrinks)
            ->first();
        return $detailDrinks;
    }

}
