<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeDrinks extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    public function drinks()
    {
        return $this->belongsTo('App\Models\Drinks');
    }

    public function getTypeDrinks()
    {
        $typeDrinks = TypeDrinks::select('type_drinks.id', 'type_drinks.name')
            ->get();
        return $typeDrinks;
    }
}
