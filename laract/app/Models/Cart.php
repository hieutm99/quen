<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;

class Cart extends Model
{
    protected $table = 'cart';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'amount', 'price', 'user_id', 'drinks_id'
    ];

    public function users()
    {
        return $this->hasMany('App\Users');
    }

    public function drinks()
    {
        return $this->hasMany('App\Models\Drinks');
    }

    public function insertCart($payload)
    {
        $cart = Cart::where('user_id', $payload->user_id)->where('drinks_id', $payload->drinks_id)->first();
        if ($cart) {
            $cart->name = $payload->name;
            $cart->image = $payload->image;
            $cart->amount += $payload->amount;
            $cart->price = $payload->price;
            $cart->save();
        } else {
            $cart = new Cart;
            $cart->name = $payload->name;
            $cart->image = $payload->image;
            $cart->amount += $payload->amount;
            $cart->price = $payload->price;
            $cart->user_id += $payload->user_id;
            $cart->drinks_id = $payload->drinks_id;
            $cart->save();
        }
        return $cart;
    }

    public function getCartItemsByIdUser($idUser)
    {
        $cartItems = Cart::select('cart.id', 'cart.name', 'cart.image', 'cart.amount', 'cart.price', 'cart.user_id as idUser', 'cart.drinks_id as idDrinks')
            ->where('cart.user_id', $idUser)
            ->get();
        return $cartItems;
    }

    public function setAmountCartItemByUser($idCart, $newAmount)
    {
        $cartItem = Cart::find($idCart);
        $cartItem->amount = $newAmount;
        $cartItem->save();
    }

    public function getTotalPriceAndAmountByIdUser($idUser)
    {
        $totalPriceAndAmount = Cart::select('user_id', DB::raw('SUM(amount) as totalAmount'), DB::raw('SUM(price*amount) as totalPrice'))
            ->where('user_id', $idUser)
            ->groupBy('user_id')
            ->first();
        return $totalPriceAndAmount;
    }

    public function totalPaymentAndTotalItemsByIdUser($idUser)
    {
        $cartItems = Cart::select('cart.id', 'cart.name', 'cart.image', 'cart.amount', 'cart.price', 'cart.user_id as idUser', 'cart.drinks_id as idDrinks')
            ->where('cart.user_id', $idUser)
            ->get();
        return $cartItems;
    }

    public function deleteCartByIdUser($idUser)
    {
        DB::beginTransaction();
        try {
            Cart::where('cart.user_id', $idUser)->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }
}
