<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Cart;
use App\Models\Orders;
use App\Models\DetailOrder;
use Illuminate\Support\Facades\DB;
use Exception;

class CartController extends Controller
{
    protected $cart;
    protected $orders;
    protected $detailOrder;

    public function __construct(
        Cart $cart,
        Orders $orders,
        DetailOrder $detailOrder
    )
    {
        $this->cart = $cart;
        $this->orders = $orders;
        $this->detailOrder = $detailOrder;
    }

    public function addToCart(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->cart->insertCart($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
        $response = ['success' => true];
        return response()->json($response, Response::HTTP_OK);
    }

    public function fetchDataCart(Request $request)
    {
        $cartItems = $this->cart->getCartItemsByIdUser($request->idUser);
        $totalPriceAndAmount = $this->cart->getTotalPriceAndAmountByIdUser($request->idUser);
        return response()->json(array(
            'cartItems' => $cartItems,
            'totalPriceAndAmount' => $totalPriceAndAmount,
        ), Response::HTTP_OK);
    }

    public function increaseItemCart(Request $request)
    {
        $newAmount = $request->newAmount + 1;
        $this->cart->setAmountCartItemByUser($request->idCart, $newAmount);
        $cartItems = $this->cart->getCartItemsByIdUser($request->idUser);
        $totalPriceAndAmount = $this->cart->getTotalPriceAndAmountByIdUser($request->idUser);
        return response()->json(array(
            'cartItems' => $cartItems,
            'totalPriceAndAmount' => $totalPriceAndAmount,
        ), Response::HTTP_OK);
    }

    public function decreaseItemCart(Request $request)
    {
        $newAmount = $request->newAmount - 1;
        $this->cart->setAmountCartItemByUser($request->idCart, $newAmount);
        $cartItems = $this->cart->getCartItemsByIdUser($request->idUser);
        $totalPriceAndAmount = $this->cart->getTotalPriceAndAmountByIdUser($request->idUser);
        return response()->json(array(
            'cartItems' => $cartItems,
            'totalPriceAndAmount' => $totalPriceAndAmount,
        ), Response::HTTP_OK);
    }

    public function clearCart(Request $request)
    {
        $this->cart->deleteCartByIdUser($request->idUser);
        $response = ['success' => true];
        return response()->json($response, Response::HTTP_OK);
    }

    public function order(Request $request)
    {
        $payload = $this->cart->getCartItemsByIdUser($request->idUser);
        $idOrder = $this->orders->insertOrder($request->idUser);
        $this->detailOrder->addMoreDetailOrder($idOrder, $payload);
        $this->cart->deleteCartByIdUser($request->idUser);
        $response = ['success' => true];
        return response()->json($response, Response::HTTP_OK);
    }
}
