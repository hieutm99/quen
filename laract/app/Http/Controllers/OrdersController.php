<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\DetailOrder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrdersController extends Controller
{
    protected $orders;
    protected $detailOrder;

    public function __construct(
        Orders $orders,
        DetailOrder $detailOrder
    ){
        $this->orders = $orders;
        $this->detailOrder = $detailOrder;
    }

    public function fetchOrders(Request $request)
    {
        $datetimeOrders = $this->orders->getDatetimeOrders($request->idUser);
        $fetchOrders = null;
        if ($datetimeOrders[0]->id){
            $fetchOrders = $this->detailOrder->getDataOrdersByIdOrder($datetimeOrders[0]->id);
        }
        return response()->json(array(
            'datetimeOrders' => $datetimeOrders,
            'fetchOrders' => $fetchOrders,
        ), Response::HTTP_OK);
    }

    public function filterOrders(Request $request){
        $fetchOrders = $this->detailOrder->getDataOrdersByIdOrder($request->idOrder);
        return response()->json($fetchOrders, Response::HTTP_OK);
    }
}
