<?php

namespace App\Http\Controllers;

use App\Models\Drinks;
use App\Models\TypeDrinks;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DrinksController extends Controller
{
    protected $drinks;
    protected $typeDrinks;

    public function __construct(
        Drinks $drinks,
        TypeDrinks $typeDrinks
    ){
        $this->drinks = $drinks;
        $this->typeDrinks = $typeDrinks;
    }

    public function fetchDrinks()
    {
        $fetchDrinks = $this->drinks->getDataDrinks();
        $typeDrinks = $this->typeDrinks->getTypeDrinks();
        return response()->json(array(
            'fetchDrinks' => $fetchDrinks,
            'typeDrinks' => $typeDrinks,
        ), Response::HTTP_OK);
    }

    public function fetchDetailDrinks(Request $request)
    {
        $detailDrinks = $this->drinks->getDetailDrinks($request->idDrinks);
        return response()->json($detailDrinks, Response::HTTP_OK);
    }
}
