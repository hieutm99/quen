<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use JWTAuthException;
use Validator;
use Exception;

class UsersController extends Controller
{
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6|max:25',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'address' => 'required',
            'password' => 'required|min:6|max:25',
            'confirm_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $payload = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'password' => \Hash::make($request->password),
            'level' => 0
        ];
        DB::beginTransaction();
        try {
            $user = new User($payload);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
        if ($user->save()) {
            $credentials = $request->only('email', 'password');
            if (!($token = JWTAuth::attempt($credentials))) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], Response::HTTP_BAD_REQUEST);
            }
            $response = ['success' => true, 'token' => $token, 'nameUser' => $user->name, 'userId' => $user->id];
            return response()->json($response, Response::HTTP_OK);
        } else {
            $response = ['success' => false, 'message' => 'Register Failed'];
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }

    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6|max:25',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user = User::where('email', $request->email)->first();
        if ($user && \Hash::check($request->password, $user->password)) {
            $credentials = $request->only('email', 'password');
            if (!($token = JWTAuth::attempt($credentials))) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], Response::HTTP_BAD_REQUEST);
            }
            $response = ['success' => true, 'token' => $token, 'nameUser' => $user->name, 'userId' => $user->id];
            return response()->json($response, Response::HTTP_OK);
        } else {
            $response = ['success' => false, 'password' => 'Incorrect password'];
            return response()->json($response, Response::HTTP_OK);
        }
    }

}
