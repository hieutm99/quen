import React from 'react';
import PropTypes from 'prop-types'
import './App.css';
import Header from './components/users/Header'
import Footer from './components/users/Footer'
import {Route} from "react-router-dom";

// Components User
import FetchDrinks from "./components/users/list_drinks";
import FetchDetailDrinks from "./components/users/detail_drinks";
import ForgotPassword from "./components/auth/forgot_password";
import ResetPassword from "./components/auth/reset_password";
import Cart from "./components/users/cart";
import OrderHistory from "./components/users/order_history";

const App = props => (
    <div className="App">
        <section className="App-body">
            <Header/>
            <Route exact path="/" component={FetchDrinks}/>
            <Route exact path="/detail_drinks" component={FetchDetailDrinks}/>
            <Route exact path='/forgot' component={ForgotPassword}/>
            <Route exact path='/reset' component={ResetPassword}/>
            <Route exact path='/cart' component={Cart}/>
            <Route exact path='/order_history' component={OrderHistory}/>
            <Footer/>
        </section>
    </div>
)

App.propTypes = {
    children: PropTypes.node,
}

export default App;
