import {fork} from 'redux-saga/effects';
import fetchDrinksSaga from './components/users/list_drinks/sagas'
import fetchDetailDrinksSaga from './components/users/detail_drinks/sagas'
import signupSaga from './components/auth/signup/sagas'
import loginSaga from './components/auth/login/sagas'
import forgotPasswordSaga from './components/auth/forgot_password/sagas'
import resetPasswordSaga from './components/auth/reset_password/sagas'
import cartSaga from './components/users/cart/sagas'
import fetchOrdersSaga from './components/users/order_history/sagas'

export default function* IndexSagas () {
    yield fork(fetchDrinksSaga)
    yield fork(fetchDetailDrinksSaga)
    yield fork(signupSaga)
    yield fork(loginSaga)
    yield fork(forgotPasswordSaga)
    yield fork(resetPasswordSaga)
    yield fork(cartSaga)
    yield fork(fetchOrdersSaga)
}
