import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import fetchDrinksReducer from './components/users/list_drinks/reducer'
import signupReducer from './components/auth/signup/reducer'
import loginReducer from './components/auth/login/reducer'
import fetchDetailDrinksReducer from './components/users/detail_drinks/reducer'
import forgotPasswordReducer from './components/auth/forgot_password/reducer'
import resetPasswordReducer from './components/auth/reset_password/reducer'
import cartReducer from './components/users/cart/reducers'
import fetchOrdersReducer from './components/users/order_history/reducers'

const IndexReducer = () => combineReducers({
    fetchDrinksReducer,
    signupReducer,
    loginReducer,
    fetchDetailDrinksReducer,
    forgotPasswordReducer,
    resetPasswordReducer,
    cartReducer,
    fetchOrdersReducer,
    form,
})
export default IndexReducer
