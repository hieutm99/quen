import {call, put, takeLatest} from 'redux-saga/effects';
import {
    loginRequest,
    loginSuccess,
    loginError
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

const loginUrl = `${config.API_SERVER_URL}user/login`;

function loginApi(email, password) {
    return axios.post(loginUrl, {
        email: email,
        password: password,
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* loginFlow(action) {
    try {
        const {email, password} = action.payload
        const response = yield call(loginApi, email, password)
        yield put(loginSuccess(response));
    } catch (error) {
        yield put(loginError(error));
    }
}

export default function* loginSaga() {
    yield takeLatest(loginRequest, loginFlow)
}
