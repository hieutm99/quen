import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'LOGIN_REQUEST',
    'LOGIN_SUCCESS',
    'LOGIN_ERROR',
    {
        prefix: 'LOGIN',
    },
);

export const {
    loginRequest,
    loginSuccess,
    loginError
} = actionCreators;
