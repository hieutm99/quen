// Libraries
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {reduxForm, Field} from 'redux-form'
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from 'reactstrap';

// Components
import {actionCreators} from './actions';
import {Link} from "react-router-dom";

// Validation redux form
const required = value => value ? undefined : 'Required'
const minlength = min => value =>
    value && value.length < min ? `Must be at least ${min}` : undefined
const minlength6 = minlength(6)
const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength25 = maxLength(25)
const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
        'Invalid email address' : undefined

// Component render redux form
const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    <div>
        <label><b>{label}</b></label>
        <div>
            <input className="form-control" {...input} placeholder={label} type={type}/>
            {touched && ((error && <p style={{color: "red"}}>* {error}</p>) || (warning &&
                <p style={{color: "red"}}>* {warning}</p>))}
        </div>
    </div>)

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        }

        this.toggleModal = this.toggleModal.bind(this);
    }

    toggleModal(e) {
        this.setState({modal: !this.state.modal})
    }

    static propTypes = {
        handleSubmit: PropTypes.func,
        loginRequest: PropTypes.func,
        loginReducer: PropTypes.shape({
            messages: PropTypes.object,
            errors: PropTypes.object,
            successful: PropTypes.bool
        }),
    }

    submit = (values) => {
        const {loginRequest} = this.props;
        loginRequest(values);
    }

    onClickCheckLoginSuccess = (successful, messages) => {
        if (successful && this.state.modal !== false && messages.success) {
            localStorage.setItem('token', messages.token)
            localStorage.setItem('name', messages.name)
            localStorage.setItem('user_id', messages.user_id)
            window.location.reload()
        }
    }

    render() {
        const {
            handleSubmit,
            loginReducer: {
                messages,
                errors,
                successful
            },
        } = this.props

        return (
            <div>
                <Button color="primary" onClick={this.toggleModal}>Đăng nhập</Button>
                <Modal isOpen={this.state.modal} toggle={true}>
                    <ModalHeader toggle={() => {
                        this.toggleModal();
                        this.props.reset()
                    }}>LOGIN</ModalHeader>
                    <div className="login">
                        <form className="login-form" onSubmit={handleSubmit(this.submit)}>
                            <ModalBody>
                                <div>
                                    <div>
                                        <Field
                                            name="email"
                                            type="email"
                                            id="email"
                                            className="email"
                                            label="Email"
                                            component={renderField}
                                            placeholder="Email"
                                            validate={[required, email]}
                                        />
                                    </div>
                                    <p>{errors.email && (<span style={{color: "red"}}>* {errors.email}</span>)}</p>
                                    <div>
                                        <Field
                                            name="password"
                                            type="password"
                                            id="password"
                                            className="password"
                                            label="Password"
                                            component={renderField}
                                            placeholder="Password"
                                            validate={[required, minlength6, maxLength25]}
                                        />
                                    </div>
                                    <p>{errors.password && (
                                        <span style={{color: "red"}}>* {errors.password}</span>)}</p>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <p><Link to="/forgot" onClick={this.toggleModal}>Forgot password</Link></p>
                                <Button color="primary" onClick={this.onClickCheckLoginSuccess(successful, messages)}>Đăng
                                    nhập</Button>
                                <Button color="secondary" onClick={() => {
                                    this.toggleModal();
                                    this.props.reset()
                                }}>Bỏ qua</Button>
                            </ModalFooter>
                        </form>
                    </div>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state,
})

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({
            ...actionCreators
        },
        dispatch
    );
}

const connected = connect(mapStateToProps, mapDispatchToProps)(Login)

const formed = reduxForm({
    form: 'loginForm',
})(connected)

export default formed
