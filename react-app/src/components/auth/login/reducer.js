import {handleActions} from 'redux-actions';
import {
    loginRequest,
    loginSuccess,
    loginError
} from './actions';

const initialState = {
    requesting: false,
    successful: false,
    messages: {},
    errors: {},
}

const reducer = handleActions(
    {
        [loginRequest]: (state = initialState, {payload}) => {
            return {
                requesting: true,
                successful: false,
                messages: {},
                errors: {},
            };
        },
        [loginSuccess]: (state = initialState, {payload}) => {
            return {
                errors: {
                    email: payload.data.email,
                    password: payload.data.password,
                },
                messages: {
                    success: payload.data.success,
                    token: payload.data.token,
                    name: payload.data.nameUser,
                    user_id: payload.data.userId
                },
                requesting: false,
                successful: true,
            };
        },
        [loginError]: (state = initialState, {payload}) => {
            return {
                errors: {},
                messages: {},
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducer
