import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'RESET_PASSWORD_REQUEST',
    'RESET_PASSWORD_SUCCESS',
    'RESET_PASSWORD_ERROR',
    {
        prefix: 'RESET_PASSWORD',
    },
);

export const {
    resetPasswordRequest,
    resetPasswordSuccess,
    resetPasswordError
} = actionCreators;
