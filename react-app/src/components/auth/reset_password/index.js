// Libraries
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {reduxForm, Field} from 'redux-form'
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";
import {
    Button,
} from 'reactstrap';

// Components
import {actionCreators} from './actions';
import Container from "reactstrap/es/Container";

// Validation redux form
const required = value => value ? undefined : 'Required'
const minlength = min => value =>
    value && value.length < min ? `Must be at least ${min}` : undefined
const minlength6 = minlength(6)
const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength25 = maxLength(25)

// Component render redux form
const renderField = ({input, label, type, disabled, meta: {touched, error, warning}}) => (
    <div>
        <label><b>{label}</b></label>
        <div>
            <input className="form-control" {...input} placeholder={label} type={type} disabled={disabled}/>
            {touched && ((error && <p style={{color: "red"}}>* {error}</p>) || (warning &&
                <p style={{color: "red"}}>* {warning}</p>))}
        </div>
    </div>)

class ResetPassword extends Component {
    static propTypes = {
        handleSubmit: PropTypes.func,
        resetPasswordRequest: PropTypes.func,
        resetPasswordReducer: PropTypes.shape({
            messages: PropTypes.object,
            errors: PropTypes.object,
            successful: PropTypes.bool
        }),
    }

    componentDidMount() {
        this.props.initialize({ email: this.props.location.state.email });
    }

    submit = (values) => {
        const {resetPasswordRequest} = this.props;
        resetPasswordRequest(values);
    }

    onClickCheckResetPasswordSuccess = (successful, messages) => {
        if (successful && messages.success) {
            this.props.history.push("/")
        }
    }

    render() {
        const {
            handleSubmit,
            resetPasswordReducer: {
                messages,
                errors,
                successful
            },
        } = this.props

        return (
            <Container>
                <h2 className="p-2">Reset password</h2>
                <div className="reset_password text-left">
                    <form className="reset_password_form" onSubmit={handleSubmit(this.submit)}>
                        <div>
                            <Field
                                name="email"
                                type="email"
                                id="email"
                                className="email"
                                label="Email"
                                component={renderField}
                                placeholder="Email"
                                props={{
                                    disabled: true
                                }}
                            />
                        </div>
                        <div>
                            <Field
                                name="verify_code"
                                type="text"
                                id="verify_code"
                                className="verify_code"
                                label="Verify Code"
                                component={renderField}
                                placeholder="Verify code"
                                validate={required}
                            />
                        </div>
                        <p>{errors.verify_code && (
                            <span style={{color: "red"}}>* {errors.verify_code}</span>)}</p>
                        <div>
                            <Field
                                name="password"
                                type="password"
                                id="password"
                                className="password"
                                label="Password"
                                component={renderField}
                                placeholder="Password"
                                validate={[required, minlength6, maxLength25]}
                            />
                        </div>
                        <p>{errors.password && (
                            <span style={{color: "red"}}>* {errors.password}</span>)}</p>
                        <div>
                            <Field
                                name="confirm_password"
                                type="password"
                                id="confirm_password"
                                className="confirm_password"
                                label="Confirm Password"
                                component={renderField}
                                placeholder="Confirm Password"
                                validate={required}
                            />
                        </div>
                        <p>{errors.confirm_password && (
                            <span style={{color: "red"}}>* {errors.confirm_password}</span>)}</p>
                        <Button color="primary"
                                onClick={this.onClickCheckResetPasswordSuccess(successful, messages)}>Submit</Button>
                    </form>
                </div>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    ...state,
})

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({
            ...actionCreators
        },
        dispatch
    );
}

const connected = connect(mapStateToProps, mapDispatchToProps)(ResetPassword)

const formed = reduxForm({
    form: 'resetPasswordForm'
})(connected)

export default formed
