import {handleActions} from 'redux-actions';
import {
    resetPasswordRequest,
    resetPasswordSuccess,
    resetPasswordError
} from './actions';

const initialState = {
    requesting: false,
    successful: false,
    messages: {},
    errors: {},
}

const reducer = handleActions(
    {
        [resetPasswordRequest]: (state = initialState, {payload}) => {
            return {
                requesting: true,
                successful: false,
                messages: {},
                errors: {},
            };
        },
        [resetPasswordSuccess]: (state = initialState, {payload}) => {
            return {
                errors: {
                    message: !payload.data.success ? payload.data.message : null,
                    verify_code: payload.data.verify_code,
                    password: payload.data.password,
                    confirm_password: payload.data.confirm_password
                },
                messages: {
                    success: payload.data.success ? payload.data.success : null,
                },
                requesting: false,
                successful: true,
            };
        },
        [resetPasswordError]: (state = initialState, {payload}) => {
            return {
                errors: {},
                messages: {},
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducer
