import {call, put, takeLatest} from 'redux-saga/effects';
import {
    resetPasswordRequest,
    resetPasswordSuccess,
    resetPasswordError
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

const resetPasswordUrl = `${config.API_SERVER_URL}reset-password`;

function resetPasswordApi(email, verify_code, password, confirm_password) {
    return axios.put(resetPasswordUrl, {
        email: email,
        verify_code: verify_code,
        password: password,
        confirm_password: confirm_password
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* resetPasswordFlow(action) {
    try {
        const {email, verify_code, password, confirm_password} = action.payload
        const response = yield call(resetPasswordApi, email, verify_code, password, confirm_password)
        yield put(resetPasswordSuccess(response));
    } catch (error) {
        yield put(resetPasswordError(error));
    }
}

export default function* forgotPasswordSaga() {
    yield takeLatest(resetPasswordRequest, resetPasswordFlow)
}
