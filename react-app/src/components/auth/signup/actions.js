import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'SIGNUP_REQUEST',
    'SIGNUP_SUCCESS',
    'SIGNUP_ERROR',
    {
        prefix: 'SIGN_UP',
    },
);

export const {
    signupRequest,
    signupSuccess,
    signupError
} = actionCreators;
