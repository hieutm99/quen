import {handleActions} from 'redux-actions';
import {
    signupRequest,
    signupSuccess,
    signupError
} from './actions';

const initialState = {
    requesting: false,
    successful: false,
    messages: {},
    errors: {},
}

const reducer = handleActions(
    {
        [signupRequest]: (state = initialState, {payload}) => {
            return {
                requesting: true,
                successful: false,
                messages: {},
                errors: {},
            };
        },
        [signupSuccess]: (state = initialState, {payload}) => {
            return {
                errors: {
                    name: payload.data.name,
                    phone: payload.data.phone,
                    address: payload.data.address,
                    email: payload.data.email,
                    password: payload.data.password,
                    confirm_password: payload.data.confirm_password
                },
                messages: {
                    success: payload.data.success,
                    token: payload.data.token,
                    name: payload.data.nameUser,
                    user_id: payload.data.userId
                },
                requesting: false,
                successful: true,
            };
        },
        [signupError]: (state = initialState, {payload}) => {
            return {
                errors: {},
                messages: {},
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducer
