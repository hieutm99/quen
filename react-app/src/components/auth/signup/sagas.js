import {call, put, takeLatest} from 'redux-saga/effects';
import {
    signupRequest,
    signupSuccess,
    signupError
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

const signupUrl = `${config.API_SERVER_URL}user/signup`;

function signupApi(address, confirm_password, name, email, password, phone) {
    return axios.post(signupUrl, {
        name: name,
        email: email,
        phone: phone,
        address: address,
        password: password,
        confirm_password: confirm_password
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* signupFlow(action) {
    try {
        const {address, confirm_password, name, email, password, phone} = action.payload
        const response = yield call(signupApi, address, confirm_password, name, email, password, phone)
        yield put(signupSuccess(response));
    } catch (error) {
        yield put(signupError(error));
    }
}

export default function* signupSaga() {
    yield takeLatest(signupRequest, signupFlow)
}
