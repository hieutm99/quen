// Libraries
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {reduxForm, Field} from 'redux-form'
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";
import {
    Button,
    Container,
} from 'reactstrap';

// Components
import {actionCreators} from './actions';

// Validation redux form
const required = value => value ? undefined : 'Required'
const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
        'Invalid email address' : undefined

// Component render redux form
const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    <div>
        <label><b>{label}</b></label>
        <div>
            <input className="form-control" {...input} placeholder={label} type={type}/>
            {touched && ((error && <p style={{color: "red"}}>* {error}</p>) || (warning &&
                <p style={{color: "red"}}>* {warning}</p>))}
        </div>
    </div>)

class ForgotPassword extends Component {
    static propTypes = {
        handleSubmit: PropTypes.func,
        forgotPasswordRequest: PropTypes.func,
        forgotPasswordReducer: PropTypes.shape({
            messages: PropTypes.object,
            errors: PropTypes.object,
            successful: PropTypes.bool
        }),
    }

    submit = (values) => {
        const {forgotPasswordRequest} = this.props;
        forgotPasswordRequest(values);
    }

    onClickCheckForgotPasswordSuccess = (successful, messages) => {
        if (successful && messages.email && messages.success) {
            this.props.history.push({
                pathname: '/reset',
                state: { email: messages.email }
            })
        }
    }

    render() {
        const {
            handleSubmit,
            forgotPasswordReducer: {
                messages,
                errors,
                successful
            },
        } = this.props

        return (
            <Container>
                <h2 className="p-2">Forgot password</h2>
                <div className="forgot_password text-left">
                    <form className="forgot_password_form" onSubmit={handleSubmit(this.submit)}>
                        <div>
                            <Field
                                name="email"
                                type="email"
                                id="email"
                                className="email"
                                label="Email"
                                component={renderField}
                                placeholder="Email"
                                validate={[required, email]}
                            />
                        </div>
                        <p>{errors.email && (<span style={{color: "red"}}>* {errors.email}</span>)}</p>
                        <Button color="primary" onClick={this.onClickCheckForgotPasswordSuccess(successful, messages)}>Submit</Button>
                    </form>
                </div>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    ...state,
})

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({
            ...actionCreators
        },
        dispatch
    );
}

const connected = connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)

const formed = reduxForm({
    form: 'forgotPasswordForm',
})(connected)

export default formed
