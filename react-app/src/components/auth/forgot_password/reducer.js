import {handleActions} from 'redux-actions';
import {
    forgotPasswordRequest,
    forgotPasswordSuccess,
    forgotPasswordError
} from './actions';

const initialState = {
    requesting: false,
    successful: false,
    messages: {},
    errors: {},
}

const reducer = handleActions(
    {
        [forgotPasswordRequest]: (state = initialState, {payload}) => {
            return {
                requesting: true,
                successful: false,
                messages: {},
                errors: {},
            };
        },
        [forgotPasswordSuccess]: (state = initialState, {payload}) => {
            return {
                errors: {
                    email: !payload.data.success ? payload.data.email : null,
                },
                messages: {
                    success: payload.data.success,
                    email: payload.data.success ? payload.data.email : null,
                },
                requesting: false,
                successful: true,
            };
        },
        [forgotPasswordError]: (state = initialState, {payload}) => {
            return {
                errors: {},
                messages: {},
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducer
