import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'FORGOT_PASSWORD_REQUEST',
    'FORGOT_PASSWORD_SUCCESS',
    'FORGOT_PASSWORD_ERROR',
    {
        prefix: 'FORGOT_PASSWORD',
    },
);

export const {
    forgotPasswordRequest,
    forgotPasswordSuccess,
    forgotPasswordError
} = actionCreators;
