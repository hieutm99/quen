import {call, put, takeLatest} from 'redux-saga/effects';
import {
    forgotPasswordRequest,
    forgotPasswordSuccess,
    forgotPasswordError
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

const forgotPasswordUrl = `${config.API_SERVER_URL}reset-password`;

function forgotPasswordApi(email) {
    return axios.post(forgotPasswordUrl, {
        email: email,
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* forgotPasswordFlow(action) {
    try {
        const {email} = action.payload
        const response = yield call(forgotPasswordApi, email)
        yield put(forgotPasswordSuccess(response));
    } catch (error) {
        yield put(forgotPasswordError(error));
    }
}

export default function* forgotPasswordSaga() {
    yield takeLatest(forgotPasswordRequest, forgotPasswordFlow)
}
