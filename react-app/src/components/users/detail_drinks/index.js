// Libraries
import React, {Component} from 'react'
import {
    Container,
    Row,
    Col,
    CardImg,
    Card,
    CardBody,
    CardTitle,
    CardText,
    Button,
    FormGroup,
    Label,
    Input,
    Form
} from 'reactstrap';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";

// Components
import {actionCreators} from './actions';

class FetchDetailDrinks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amountOrder: 1,
            priceOrder: 0
        }

        this.onClickIncreaseNumber = this.onClickIncreaseNumber.bind(this)
        this.onClickDecreaseNumber = this.onClickDecreaseNumber.bind(this)
        this.onClickAddToCart = this.onClickAddToCart.bind(this)
    }

    static propTypes = {
        addToCartPending: PropTypes.func,
        fetchDetailDrinksPending: PropTypes.func,
        fetchDetailDrinksReducer: PropTypes.shape({
            dataDrinks: PropTypes.object,
        }),
    }

    componentDidMount() {
        const {fetchDetailDrinksPending} = this.props;
        fetchDetailDrinksPending(this.props.location.state);
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    onClickIncreaseNumber(unitPrice) {
        this.setState(state => {
                return {
                    amountOrder: state.amountOrder + 1,
                    priceOrder: state.priceOrder === 0 ? state.priceOrder = unitPrice + unitPrice : state.priceOrder + unitPrice
                }
            }
        )
    }

    onClickDecreaseNumber(unitPrice) {
        if (this.state.amountOrder > 1) {
            this.setState(state => {
                    return {
                        amountOrder: state.amountOrder - 1,
                        priceOrder: state.priceOrder - unitPrice
                    }
                }
            )
        }
    }

    onClickAddToCart(drinks) {
        let userId = localStorage.getItem("user_id") ? localStorage.getItem("user_id") : null;
        if (userId){
            const objDrinks = {
                name: drinks.name,
                image: drinks.image,
                amount: this.state.amountOrder,
                price: drinks.price,
                user_id: Number(userId),
                drinks_id: drinks.id,
            }
            const {addToCartPending} = this.props;
            addToCartPending(objDrinks);
        }
    }

    render() {
        console.log('render')
        const {
            fetchDetailDrinksReducer: {
                dataDrinks,
            }
        } = this.props

        return (
            <div>
                <h2 className="p-2">Detail drinks</h2>
                <Container>
                    <Row>
                        <Col xs={12} sm={6}>
                            <Card body outline color="secondary">
                                <CardImg top width="100%" src={dataDrinks.image} alt="Card image cap"/>
                            </Card>
                        </Col>
                        <Col xs={12} sm={6}>
                            <Card body outline className="text-left">
                                <CardTitle><h4>{dataDrinks.name}</h4></CardTitle>
                                <CardBody>
                                    <CardText><b>Type:</b> <i
                                        style={{color: "blue"}}>#{dataDrinks.typeName}</i></CardText>
                                    <CardText>
                                        <b>Price: </b>
                                        {this.formatNumber(String(this.state.priceOrder === 0 ? dataDrinks.price : this.state.priceOrder)) + ' VNĐ'}
                                    </CardText>
                                    <Form>
                                        <FormGroup row>
                                            <Col xs={12} md={7}>
                                                <Button className="w-auto d-inline-block float-left mr-2" color="danger"
                                                        onClick={() => this.onClickDecreaseNumber(dataDrinks.price)}>-</Button>
                                                <Input className="border-dark d-inline-block float-left w-25"
                                                       type="text" name="amountOrder" id="amountOrder"
                                                       value={this.state.amountOrder}/>
                                                <Button className="w-auto d-inline-block float-left ml-2" color="danger"
                                                        onClick={() => this.onClickIncreaseNumber(dataDrinks.price)}>+</Button>
                                            </Col>
                                        </FormGroup>
                                        <Button className="form-control" color="success" onClick={() => this.onClickAddToCart(dataDrinks)}>Add to card</Button>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs={12} className="mb-3">
                            <FormGroup className="text-left mt-2">
                                <Label><b>Description: </b></Label>
                                <Input disabled type="textarea" value={dataDrinks.description}/>
                            </FormGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state,
})

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({
            ...actionCreators
        },
        dispatch
    );
}

const connected = connect(mapStateToProps, mapDispatchToProps)(FetchDetailDrinks)

export default connected
