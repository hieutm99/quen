import {call, put, takeLatest} from 'redux-saga/effects';
import {
    fetchDetailDrinksPending,
    fetchDetailDrinksSuccess,
    fetchDetailDrinksError,
    addToCartPending,
    addToCartSuccess,
    addToCartError,
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

function fetchDetailDrinksApi(idDrinks) {
    const fetchDetailDrinksUrl = `${config.API_SERVER_URL}drinks/fetch-detail-drinks?idDrinks=${idDrinks}`;
    return axios.get(fetchDetailDrinksUrl,{
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

function addToCartApi(name, image, amount, price, user_id, drinks_id) {
    const addToCartUrl = `${config.API_SERVER_URL}cart/add-to-cart`;
    return axios.post(addToCartUrl,{
        name: name,
        image: image,
        amount: amount,
        price: price,
        user_id: user_id,
        drinks_id: drinks_id
    },{
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* fetchDetailDrinksFlow(action) {
    try {
        const {idDrinks} = action.payload
        const response = yield call(fetchDetailDrinksApi, idDrinks)
        yield put(fetchDetailDrinksSuccess(response));
    } catch (error) {
        yield put(fetchDetailDrinksError(error));
    }
}

export function* addToCartFlow(action) {
    try {
        const {name, image, amount, price, user_id, drinks_id} = action.payload
        console.log('data: ', action.payload, ' - amount: ', amount)
        const response = yield call(addToCartApi, name, image, amount, price, user_id, drinks_id)
        yield put(addToCartSuccess(response));
    } catch (error) {
        yield put(addToCartError(error));
    }
}

export default function* fetchDetailDrinksSaga() {
    yield takeLatest(fetchDetailDrinksPending, fetchDetailDrinksFlow)
    yield takeLatest(addToCartPending, addToCartFlow)
}
