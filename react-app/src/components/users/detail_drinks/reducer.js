import {handleActions} from 'redux-actions';
import {
    fetchDetailDrinksPending,
    fetchDetailDrinksSuccess,
    fetchDetailDrinksError,
    addToCartPending,
    addToCartSuccess,
    addToCartError,
} from './actions';

const initialState = {
    dataDrinks: {},
    requesting: false,
    successful: false,
}

const reducer = handleActions(
    {
        [fetchDetailDrinksPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                dataDrinks: {},
                requesting: true,
                successful: false,
            };
        },
        [fetchDetailDrinksSuccess]: (state = initialState, {payload}) => {
            return {
                ...state,
                dataDrinks: payload.data,
                requesting: false,
                successful: true,
            }
        },
        [fetchDetailDrinksError]: (state = initialState, {payload}) => {
            return {
                ...state,
                dataDrinks: {},
                requesting: false,
                successful: false,
            };
        },
        [addToCartPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: true,
                successful: false,
            };
        },
        [addToCartSuccess]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: false,
                successful: true,
            }
        },
        [addToCartError]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducer
