import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'FETCH_DETAIL_DRINKS_PENDING',
    'FETCH_DETAIL_DRINKS_SUCCESS',
    'FETCH_DETAIL_DRINKS_ERROR',
    'ADD_TO_CART_PENDING',
    'ADD_TO_CART_SUCCESS',
    'ADD_TO_CART_ERROR',
    {
        prefix: 'FETCH_DETAIL_DRINKS',
    },
);

export const {
    fetchDetailDrinksPending,
    fetchDetailDrinksSuccess,
    fetchDetailDrinksError,
    addToCartPending,
    addToCartSuccess,
    addToCartError,
} = actionCreators;
