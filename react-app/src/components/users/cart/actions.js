import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'FETCH_DATA_CART_PENDING',
    'FETCH_DATA_CART_SUCCESS',
    'FETCH_DATA_CART_ERROR',
    'INCREASE_ITEM_CART_PENDING',
    'INCREASE_ITEM_CART_SUCCESS',
    'INCREASE_ITEM_CART_ERROR',
    'DECREASE_ITEM_CART_PENDING',
    'DECREASE_ITEM_CART_SUCCESS',
    'DECREASE_ITEM_CART_ERROR',
    'CLEAR_CART_PENDING',
    'CLEAR_CART_SUCCESS',
    'CLEAR_CART_ERROR',
    'ORDER_PENDING',
    'ORDER_SUCCESS',
    'ORDER_ERROR',
    {
        prefix: 'SHOPPING_CART',
    },
);

export const {
    fetchDataCartPending,
    fetchDataCartSuccess,
    fetchDataCartError,
    increaseItemCartPending,
    increaseItemCartSuccess,
    increaseItemCartError,
    decreaseItemCartPending,
    decreaseItemCartSuccess,
    decreaseItemCartError,
    clearCartPending,
    clearCartSuccess,
    clearCartError,
    orderPending,
    orderSuccess,
    orderError,
} = actionCreators;
