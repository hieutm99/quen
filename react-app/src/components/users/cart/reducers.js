import {handleActions} from 'redux-actions';
import {
    fetchDataCartPending,
    fetchDataCartSuccess,
    fetchDataCartError,
    increaseItemCartPending,
    increaseItemCartSuccess,
    increaseItemCartError,
    decreaseItemCartPending,
    decreaseItemCartSuccess,
    decreaseItemCartError,
    clearCartPending,
    clearCartSuccess,
    clearCartError,
    orderPending,
    orderSuccess,
    orderError,
} from './actions';

const initialState = {
    listCart: [],
    totalPriceAndAmount: {},
    requesting: false,
    successful: false,
}

const reducer = handleActions(
    {
        [fetchDataCartPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: true,
                successful: false,
            };
        },
        [fetchDataCartSuccess]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: payload.data.cartItems,
                totalPriceAndAmount: payload.data.totalPriceAndAmount,
                requesting: false,
                successful: true,
            }
        },
        [fetchDataCartError]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: false,
                successful: false,
            };
        },
        [increaseItemCartPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: true,
                successful: false,
            };
        },
        [increaseItemCartSuccess]: (state = initialState, {payload}) => {
            console.log('increaseItemCartSuccess: ', payload.data)
            return {
                ...state,
                listCart: payload.data.cartItems,
                totalPriceAndAmount: payload.data.totalPriceAndAmount,
                requesting: false,
                successful: true,
            }
        },
        [increaseItemCartError]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: false,
                successful: false,
            };
        },
        [decreaseItemCartPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: true,
                successful: false,
            };
        },
        [decreaseItemCartSuccess]: (state = initialState, {payload}) => {
            console.log('decreaseItemCartSuccess: ', )
            return {
                ...state,
                listCart: payload.data.cartItems,
                totalPriceAndAmount: payload.data.totalPriceAndAmount,
                requesting: false,
                successful: true,
            }
        },
        [decreaseItemCartError]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: false,
                successful: false,
            };
        },
        [clearCartPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: true,
                successful: false,
            };
        },
        [clearCartSuccess]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: false,
                successful: true,
            }
        },
        [clearCartError]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: false,
                successful: false,
            };
        },
        [orderPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: true,
                successful: false,
            };
        },
        [orderSuccess]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: false,
                successful: true,
            }
        },
        [orderError]: (state = initialState, {payload}) => {
            return {
                ...state,
                listCart: [],
                totalPriceAndAmount: {},
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducer
