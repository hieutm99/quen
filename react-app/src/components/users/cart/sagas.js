import {call, put, takeLatest} from 'redux-saga/effects';
import {
    fetchDataCartPending,
    fetchDataCartSuccess,
    fetchDataCartError,
    increaseItemCartPending,
    increaseItemCartSuccess,
    increaseItemCartError,
    decreaseItemCartPending,
    decreaseItemCartSuccess,
    decreaseItemCartError,
    clearCartPending,
    clearCartSuccess,
    clearCartError,
    orderPending,
    orderSuccess,
    orderError,
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

function fetchDataCartApi(idUser) {
    const fetchDataCartUrl = `${config.API_SERVER_URL}cart/fetch-data-cart?idUser=${idUser}`;
    console.log('idUser', idUser)
    return axios.get(fetchDataCartUrl, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

function clearCartApi(idUser) {
    const clearCartUrl = `${config.API_SERVER_URL}cart/clear-cart`;
    return axios.post(clearCartUrl, {
        idUser: idUser
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

function orderApi(idUser) {
    const orderUrl = `${config.API_SERVER_URL}cart/order`;
    return axios.post(orderUrl, {
        idUser: idUser
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

function increaseItemCartApi(idCart, idUser, newAmount) {
    const increaseItemCartUrl = `${config.API_SERVER_URL}cart/increase-item-cart`;
    return axios.post(increaseItemCartUrl, {
        idCart: idCart,
        idUser: idUser,
        newAmount: newAmount,
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

function decreaseItemCartApi(idCart, idUser, newAmount) {
    const decreaseItemCartUrl = `${config.API_SERVER_URL}cart/decrease-item-cart`;
    return axios.post(decreaseItemCartUrl, {
        idCart: idCart,
        idUser: idUser,
        newAmount: newAmount
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* fetchDataCartFlow(action) {
    try {
        const {idUser} = action.payload
        const response = yield call(fetchDataCartApi, idUser)
        yield put(fetchDataCartSuccess(response));
    } catch (error) {
        yield put(fetchDataCartError(error));
    }
}

export function* clearCartFlow(action) {
    try {
        const {idUser} = action.payload
        const response = yield call(clearCartApi, idUser)
        yield put(clearCartSuccess(response));
    } catch (error) {
        yield put(clearCartError(error));
    }
}

export function* orderFlow(action) {
    try {
        const {idUser} = action.payload
        console.log("co r:" , idUser)
        const response = yield call(orderApi, idUser)
        yield put(orderSuccess(response));
    } catch (error) {
        yield put(orderError(error));
    }
}

export function* increaseItemCartFlow(action) {
    try {
        const {idCart, idUser, newAmount} = action.payload
        const response = yield call(increaseItemCartApi, idCart, idUser, newAmount)
        yield put(increaseItemCartSuccess(response));
    } catch (error) {
        yield put(increaseItemCartError(error));
    }
}

export function* decreaseItemCartFlow(action) {
    try {
        const {idCart, idUser, newAmount} = action.payload
        const response = yield call(decreaseItemCartApi, idCart, idUser, newAmount)
        yield put(decreaseItemCartSuccess(response));
    } catch (error) {
        yield put(decreaseItemCartError(error));
    }
}

export default function* cartSaga() {
    yield takeLatest(fetchDataCartPending, fetchDataCartFlow)
    yield takeLatest(clearCartPending, clearCartFlow)
    yield takeLatest(orderPending, orderFlow)
    yield takeLatest(increaseItemCartPending, increaseItemCartFlow)
    yield takeLatest(decreaseItemCartPending, decreaseItemCartFlow)
}
