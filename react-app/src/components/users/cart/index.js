// Libraries
import React, {Component} from 'react'
import {
    Table,
    Row,
    Col,
    Button,
    CardImg,
    Card
} from 'reactstrap';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";

// Components
import {actionCreators} from './actions';

class FetchDetailDrinks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idUser: null,
            totalPayment: 0,
            totalItems: 0,
        }

        this.onClickIncreaseNumber = this.onClickIncreaseNumber.bind(this)
        this.onClickDecreaseNumber = this.onClickDecreaseNumber.bind(this)
        this.onClickClearCartItems = this.onClickClearCartItems.bind(this)
    }

    static propTypes = {
        fetchDataCartPending: PropTypes.func,
        increaseItemCartPending: PropTypes.func,
        decreaseItemCartPending: PropTypes.func,
        clearCartPending: PropTypes.func,
        orderPending: PropTypes.func,
        cartReducer: PropTypes.shape({
            listCart: PropTypes.array,
            totalPriceAndAmount: PropTypes.object
        }),
    }

    componentDidMount() {
        const idUser = localStorage.getItem("user_id") ? localStorage.getItem("user_id") : this.state.idUser;
        this.setState({
            idUser
        })
        if (idUser) {
            const {fetchDataCartPending} = this.props;
            fetchDataCartPending({idUser: idUser});
        }
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    onClickIncreaseNumber(idCart, idUser, newAmount) {
        if (idCart && newAmount) {
            const {increaseItemCartPending} = this.props;
            increaseItemCartPending({idCart: idCart, idUser: idUser, newAmount: newAmount});
        }
    }

    onClickDecreaseNumber(idCart, idUser, newAmount) {
        if (idCart && newAmount) {
            const {decreaseItemCartPending} = this.props;
            decreaseItemCartPending({idCart: idCart, idUser: idUser, newAmount: newAmount});
        }
    }

    onClickClearCartItems(idUser) {
        if (idUser) {
            const {clearCartPending} = this.props;
            clearCartPending({idUser: idUser});
        }
    }

    onClickOrder(idUser) {
        if (idUser) {
            const {orderPending} = this.props;
            orderPending({idUser: idUser});
        }
    }

    render() {
        const {
            cartReducer: {
                listCart,
                totalPriceAndAmount,
            }
        } = this.props
        let totalAmount = null;
        let totalPrice = null;
        if (totalPriceAndAmount){
            totalAmount = Number(totalPriceAndAmount.totalAmount);
            totalPrice = Number(totalPriceAndAmount.totalPrice);
        }
        console.log('props: ', this.props)
        console.log('render')
        return (
            <div>
                <h2 className="p-2">Cart</h2>
                {listCart.length > 0 && totalPriceAndAmount ?
                    <Row className="text-left">
                        <Col xs={12} md={9}>
                            <Table bordered id="cart_items">
                                <tbody>
                                {listCart.map((item, index) => (
                                    <tr key={index}>
                                        <td width={"20%"}>
                                            <Card body outline color="secondary">
                                                <CardImg top width="100%"
                                                         src={item.image}
                                                         alt="Card image cap"/>
                                            </Card>
                                        </td>
                                        <td>
                                            <h4>{item.name}</h4>
                                            <p>Price: {this.formatNumber(item.price) + ' VNĐ'} </p>
                                        </td>
                                        <td>Qty: <span className="item_qty">{item.amount}</span></td>
                                        <td>
                                            <Button className="w-auto d-inline-block float-left mr-2" color="danger"
                                                    onClick={() => this.onClickIncreaseNumber(item.id, item.idUser, item.amount)}>+</Button>
                                            <Button className="w-auto d-inline-block float-left ml-2" color="danger"
                                                    onClick={() => this.onClickDecreaseNumber(item.id, item.idUser, item.amount)}>-</Button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                                <tfoot className="text-right">
                                <tr>
                                    <td colSpan={4}>
                                        <Button className="m-2" color="secondary"
                                                onClick={() => this.onClickClearCartItems(this.state.idUser)}>CLEAR</Button>
                                    </td>
                                </tr>
                                </tfoot>
                            </Table>
                        </Col>
                        <Col xs={12} md={2}>
                            <Card body outline color="secondary" className="p-3">
                                <p style={{fontSize: 20}}>Total Items <br/>
                                    <b>{this.state.totalItems === 0 ? totalAmount : this.state.totalItems}</b></p>
                                <p style={{fontSize: 20}}>Total Payment <br/>
                                    <b>{this.formatNumber(this.state.totalPayment === 0 ? totalPrice : this.state.totalPayment) + ' VNĐ'}</b>
                                </p>
                                <div className="mb-0 pt-5">
                                    <Button className="form-control" color="primary"
                                            onClick={() => this.onClickOrder(this.state.idUser)}>ORDER</Button>
                                </div>
                            </Card>
                        </Col>
                    </Row>
                    :
                    <Row>
                        <Col>
                            <Table>
                                <tr>
                                    <td>
                                        Empty
                                    </td>
                                </tr>
                            </Table>
                        </Col>
                    </Row>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state,
})

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({
            ...actionCreators
        },
        dispatch
    );
}

const connected = connect(mapStateToProps, mapDispatchToProps)(FetchDetailDrinks)

export default connected
