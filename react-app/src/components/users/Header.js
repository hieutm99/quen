import React, {useState} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import {FaShoppingCart} from 'react-icons/fa';
import {Link} from "react-router-dom";

import ModalSignup from '../auth/signup/index'
import ModalLogin from '../auth/login/index'

const Header = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    function logout() {
        localStorage.removeItem("token")
        localStorage.removeItem("name")
        window.location.reload()
    }

    return (
        <div>
            <Navbar color="dark" dark expand="md">
                <NavbarBrand href="/"><h1><i>Quện</i></h1><b style={{fontSize: 15}}>(Healthy drink)</b></NavbarBrand>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar className="text-right">
                    <Nav className="ml-auto" navbar>
                        {localStorage.getItem("token") === null ?
                            <NavItem>
                                <ModalLogin buttonLabel={"Đăng nhập"}/>
                                <ModalSignup/>
                            </NavItem>
                            :
                            <NavItem>
                                <UncontrolledDropdown nav inNavbar className="float-right d-inline-block">
                                    <DropdownToggle nav caret className="text-white">
                                        <i>Hello: <span style={{color: "red"}}>{localStorage.getItem("name")}</span></i>
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <Link to="/cart">
                                            <DropdownItem>
                                                <FaShoppingCart/> Giỏ hàng
                                            </DropdownItem>
                                        </Link>
                                        <Link to="/order_history">
                                            <DropdownItem>
                                                Lịch sử đặt hàng
                                            </DropdownItem>
                                        </Link>
                                        <DropdownItem divider/>
                                        <DropdownItem onClick={logout}>
                                            Đăng xuất
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </NavItem>
                        }
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );
}

export default Header;