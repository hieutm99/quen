import {call, put, takeLatest} from 'redux-saga/effects';
import {
    fetchOrdersPending,
    fetchOrdersSuccess,
    fetchOrdersError,
    filterOrdersPending,
    filterOrdersSuccess,
    filterOrdersError
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

function fetchOrdersApi(idUser) {
    const fetchOrdersUrl = `${config.API_SERVER_URL}order/order-history?idUser=${idUser}`;
    return axios.get(fetchOrdersUrl, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

function filterOrdersApi(idOrder, pageNumber = 1) {
    const filterOrdersUrl = `${config.API_SERVER_URL}order/filter-order-history?idOrder=${idOrder}&page=${pageNumber}`;
    return axios.get(filterOrdersUrl, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* fetchOrdersFlow(action) {
    try {
        const {idUser} = action.payload
        const response = yield call(fetchOrdersApi, idUser)
        yield put(fetchOrdersSuccess(response));
    } catch (error) {
        yield put(fetchOrdersError(error));
    }
}

export function* filterOrdersFlow(action) {
    try {
        const {idOrder, pageNumber} = action.payload
        const response = yield call(filterOrdersApi, idOrder, pageNumber)
        yield put(filterOrdersSuccess(response));
    } catch (error) {
        yield put(filterOrdersError(error));
    }
}

export default function* fetchDrinksSaga() {
    yield takeLatest(fetchOrdersPending, fetchOrdersFlow)
    yield takeLatest(filterOrdersPending, filterOrdersFlow)
}
