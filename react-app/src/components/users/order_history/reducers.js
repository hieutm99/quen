import {handleActions} from 'redux-actions';
import {
    fetchOrdersPending,
    fetchOrdersSuccess,
    fetchOrdersError,
    filterOrdersPending,
    filterOrdersSuccess,
    filterOrdersError
} from './actions';

const initialState = {
    listOrder: [],
    listDatetimeOrder: [],
    configPagination: {},
    requesting: false,
    successful: false,
}

const reducers = handleActions(
    {
        [fetchOrdersPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: true,
                successful: false,
            };
        },
        [fetchOrdersSuccess]: (state = initialState, {payload}) => {
            return {
                ...state,
                listOrder: payload.data.fetchOrders.data,
                listDatetimeOrder: payload.data.datetimeOrders,
                configPagination: payload.data.fetchOrders,
                requesting: false,
                successful: true,
            }
        },
        [fetchOrdersError]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: false,
                successful: false,
            };
        },
        [filterOrdersPending]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: true,
                successful: false,
            };
        },
        [filterOrdersSuccess]: (state = initialState, {payload}) => {
            console.log('reducer: ', payload.data)
            return {
                ...state,
                listOrder: payload.data.data,
                configPagination: payload.data,
                requesting: false,
                successful: true,
            }
        },
        [filterOrdersError]: (state = initialState, {payload}) => {
            return {
                ...state,
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducers
