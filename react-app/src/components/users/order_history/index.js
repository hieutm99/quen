// Libraries
import React, {Component} from 'react'
import {
    Card,
    CardImg,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Table
} from 'reactstrap';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";
import Pagination from "react-js-pagination";

// Components
import {actionCreators} from './actions';

class FetchOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idUser: null,
            queryDatetime: null,
        }
        this.handleSelectDatetimeChange = this.handleSelectDatetimeChange.bind(this);
    }

    static propTypes = {
        fetchOrdersPending: PropTypes.func,
        filterOrdersPending: PropTypes.func,
        fetchOrdersReducer: PropTypes.shape({
            listOrder: PropTypes.array,
            listDatetimeOrder: PropTypes.array,
            configPagination: PropTypes.object
        }),
    }

    componentDidMount() {
        const idUser = localStorage.getItem("user_id") ? localStorage.getItem("user_id") : this.state.idUser;
        this.setState({
            idUser
        })
        if (idUser) {
            const {fetchOrdersPending} = this.props;
            fetchOrdersPending({idUser: idUser});
        }
    }

    handleSelectDatetimeChange(e) {
        this.setState({queryDatetime: e.target.value});
        const {filterOrdersPending} = this.props;
        filterOrdersPending({idOrder: e.target.value});
    };

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    handlePageChange(pageNumber) {
        console.log(pageNumber)
        console.log('props page change: ', this.props.fetchOrdersReducer.listDatetimeOrder[0].id)
        const idOrder = this.state.queryDatetime === null ? this.props.fetchOrdersReducer.listDatetimeOrder[0].id : this.state.queryDatetime;
        const {filterOrdersPending} = this.props;
        filterOrdersPending({idOrder: idOrder, pageNumber: pageNumber});
    }

    render() {
        const {
            fetchOrdersReducer: {
                listOrder,
                listDatetimeOrder,
                configPagination
            },
        } = this.props
        console.log('listDatetimeOrder: ', listDatetimeOrder)

        return (
            <div>
                <h2 className="p-2">Order history</h2>
                <Container>
                    <Row>
                        <Col className="p-3 mb-2" style={{backgroundColor: "#844d0c"}}>
                            <Form>
                                <FormGroup row>
                                    <Col xs={4}>
                                        <Input value={this.state.queryDatetime}
                                               onChange={this.handleSelectDatetimeChange}
                                               type="select"
                                               name="selectType" id="selectType">
                                            {listDatetimeOrder.map((datetime, index) => (
                                                <option key={index} value={datetime.id}>{datetime.date_order}</option>
                                            ))}
                                        </Input>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="pb-3">
                            <Table bordered>
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Amount</th>
                                    <th>Total price</th>
                                </tr>
                                </thead>
                                <tbody>
                                {listOrder.map((order, index) => (
                                    <tr key={index}>
                                        <th scope="row">{index + 1}</th>
                                        <td>{order.name}</td>
                                        <td width={"12%"}>
                                            <Card body outline color="secondary">
                                                <CardImg top width="100%"
                                                         src={order.image}
                                                         alt="Card image cap"/>
                                            </Card>
                                        </td>
                                        <td>{order.amount_order}</td>
                                        <td>{this.formatNumber(order.amount_order * order.price_order) + ' VNĐ'}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </Table>
                            <div className="d-flex justify-content-center">
                                <Pagination
                                    activePage={configPagination.current_page}
                                    itemsCountPerPage={configPagination.per_page}
                                    totalItemsCount={configPagination.total}
                                    pageRangeDisplayed={configPagination.pageRangeDisplayed}
                                    onChange={this.handlePageChange.bind(this)}
                                    itemClass={'page-item'}
                                    linkClass={'page-link'}
                                />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state,
})

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({
            ...actionCreators
        },
        dispatch
    );
}

const connected = connect(mapStateToProps, mapDispatchToProps)(FetchOrders)

export default connected
