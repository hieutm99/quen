import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'FETCH_ORDERS_PENDING',
    'FETCH_ORDERS_SUCCESS',
    'FETCH_ORDERS_ERROR',
    'FILTER_ORDERS_PENDING',
    'FILTER_ORDERS_SUCCESS',
    'FILTER_ORDERS_ERROR',
    {
        prefix: 'FETCH_DRINKS',
    },
);

export const {
    fetchOrdersPending,
    fetchOrdersSuccess,
    fetchOrdersError,
    filterOrdersPending,
    filterOrdersSuccess,
    filterOrdersError
} = actionCreators;
