import React, {useState} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
    NavbarText
} from 'reactstrap';
import { FaFacebookSquare, FaPhoneSquareAlt } from 'react-icons/fa';

const Footer = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar color="dark" dark expand="md">
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar className="text-right">
                    <Nav className="m-auto text-center text-white d-inline-block" navbar>
                        <NavItem>
                            <h1><i>Quện</i></h1>
                            <p><u>Không Phải Hoa Quá Tươi KHÔNG Bán <br/>
                                Không Đường Sirô, Hóa Học <br/>
                                Không Rẻ Nhất Thì Cũng Không Đâu Rẻ Bằng</u></p>
                            <Nav className="d-inline-block p-2">
                                <NavItem>
                                    <NavLink href="https://github.com/reactstrap/reactstrap"><b
                                        style={{color: "white"}}><FaFacebookSquare style={{color: "#0e8cf1"}} /> HUY PHẠM</b></NavLink>
                                    <NavbarText href="https://github.com/reactstrap/reactstrap"><b
                                        style={{color: "white"}}><FaPhoneSquareAlt style={{color: "#6da880"}} /> 0369.169.525</b></NavbarText>
                                </NavItem>
                            </Nav>
                            <Nav className="d-inline-block p-2">
                                <NavItem>
                                    <NavLink href="https://github.com/reactstrap/reactstrap"><b
                                        style={{color: "white"}}><FaFacebookSquare style={{color: "#0e8cf1"}} /> THẮNG RÔ</b></NavLink>
                                    <NavbarText href="https://github.com/reactstrap/reactstrap"><b
                                        style={{color: "white"}}><FaPhoneSquareAlt style={{color: "#6da880"}} /> 0981.985.633</b></NavbarText>
                                </NavItem>
                            </Nav>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );
}

export default Footer;