import {call, put, takeLatest} from 'redux-saga/effects';
import {
    fetchDrinksPending,
    fetchDrinksSuccess,
    fetchDrinksError
} from './actions';
import axios from 'axios';
import config from "../../../config/config";

const fetchDrinksUrl = `${config.API_SERVER_URL}drinks/fetch-drinks`;

function fetchDrinksApi() {
    return axios.get(fetchDrinksUrl, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(json => json)
        .catch((error) => {
            throw error
        })
}

export function* fetchDrinksFlow() {
    try {
        const response = yield call(fetchDrinksApi)
        yield put(fetchDrinksSuccess(response));
    } catch (error) {
        yield put(fetchDrinksError(error));
    }
}

export default function* fetchDrinksSaga() {
    yield takeLatest(fetchDrinksPending, fetchDrinksFlow)
}
