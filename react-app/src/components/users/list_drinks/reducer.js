import {handleActions} from 'redux-actions';
import {
    fetchDrinksPending,
    fetchDrinksSuccess,
    fetchDrinksError
} from './actions';

const initialState = {
    listDrinks: [],
    listTypeDrinks: [],
    requesting: false,
    successful: false,
}

const reducer = handleActions(
    {
        [fetchDrinksPending]: (state = initialState, {payload}) => {
            return {
                listDrinks: [],
                listTypeDrinks: [],
                requesting: true,
                successful: false,
            };
        },
        [fetchDrinksSuccess]: (state = initialState, {payload}) => {
            return {
                listDrinks: payload.data.fetchDrinks,
                listTypeDrinks: payload.data.typeDrinks,
                requesting: false,
                successful: true,
            }
        },
        [fetchDrinksError]: (state = initialState, {payload}) => {
            return {
                listDrinks: [],
                listTypeDrinks: [],
                requesting: false,
                successful: false,
            };
        },
    },
    initialState
);

export default reducer
