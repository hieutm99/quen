import {createActions} from 'redux-actions';

export const actionCreators = createActions(
    'FETCH_DRINKS_PENDING',
    'FETCH_DRINKS_SUCCESS',
    'FETCH_DRINKS_ERROR',
    {
        prefix: 'FETCH_DRINKS',
    },
);

export const {
    fetchDrinksPending,
    fetchDrinksSuccess,
    fetchDrinksError
} = actionCreators;
