// Libraries
import React, {Component} from 'react'
import {
    Card,
    Button,
    CardTitle,
    CardText,
    CardImg,
    CardBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input
} from 'reactstrap';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";
import {Link} from "react-router-dom";

// Css
import './css/listDrinks.scss'

// Components
import {actionCreators} from './actions';

class FetchDrinks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            queryName: "",
            queryType: 1,
            filteredData: [],
            currentPage: 1,
            newsPerPage: 15
        }
        this.handleInputNameChange = this.handleInputNameChange.bind(this);
        this.handleSelectTypeChange = this.handleSelectTypeChange.bind(this);
        this.onClickSearch = this.onClickSearch.bind(this);
    }

    static propTypes = {
        fetchDrinksPending: PropTypes.func,
        fetchDrinksReducer: PropTypes.shape({
            listDrinks: PropTypes.array,
            listTypeDrinks: PropTypes.array,
        }),
    }

    componentDidMount() {
        const {fetchDrinksPending} = this.props;
        fetchDrinksPending();
    }

    handleInputNameChange(e) {
        this.setState({queryName: e.target.value});
    };

    handleSelectTypeChange(e) {
        this.setState({queryType: e.target.value});
    };

    onClickSearch() {
        const queryName = this.state.queryName.trim();
        const queryType = this.state.queryType;
        this.setState(() => {
            const filteredData = this.props.fetchDrinksReducer.listDrinks.filter(element => {
                return element.name.toLowerCase().includes(queryName.toLowerCase());
            });
            return {
                queryName,
                filteredData
            };
        });
        this.setState(prevState => {
            let filteredData = prevState.filteredData.filter(element => {
                return String(element.typeId).includes(queryType);
            });
            return {
                queryType,
                filteredData
            };
        });
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    chosePage = (event) => {
        this.setState({
            currentPage: Number(event.target.id)
        });
    }

    render() {
        const {
            fetchDrinksReducer: {
                listDrinks,
                listTypeDrinks
            },
        } = this.props
        let filterDrinks = this.state.filteredData.length > 0 ? this.state.filteredData : listDrinks;

        const currentPage = this.state.currentPage;
        const newsPerPage = this.state.newsPerPage;
        const indexOfLastNews = currentPage * newsPerPage;
        const indexOfFirstNews = indexOfLastNews - newsPerPage;
        const currentTodos = filterDrinks.slice(indexOfFirstNews, indexOfLastNews);

        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(filterDrinks.length / newsPerPage); i++) {
            pageNumbers.push(i);
        }

        return (
            <div>
                <h2 className="p-2">List drinks</h2>
                <Container>
                    <Row>
                        <Col className="p-3 mb-2" style={{backgroundColor: "#844d0c"}}>
                            <Form>
                                <FormGroup row>
                                    <Col xs={4}>
                                        <Input value={this.state.queryName} onChange={this.handleInputNameChange}
                                               type="text"
                                               name="inputName" id="inputName" placeholder="Drinks name"/>
                                    </Col>
                                    <Col xs={4}>
                                        <Input value={this.state.queryType}
                                               onChange={this.handleSelectTypeChange}
                                               type="select"
                                               name="selectType" id="selectType">
                                            {listTypeDrinks.map((type, index) => (
                                                <option key={index} value={type.id}>{type.name}</option>
                                            ))}
                                        </Input>
                                    </Col>
                                    <Col xs={{size: 2, offset: 2}}>
                                        <Button className="form-control" color="secondary"
                                                onClick={this.onClickSearch}>Search</Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        {currentTodos.map((drinks, index) => (
                            <Col xs="6" sm="4" className="pb-3" key={index}>
                                <Card body outline color="secondary">
                                    <CardImg top width="100%" src={drinks.image} alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle><h5>{drinks.name}</h5></CardTitle>
                                        <CardText>{this.formatNumber(drinks.price) + ' VNĐ'}</CardText>
                                    </CardBody>
                                    <Link to={{
                                        pathname: '/detail_drinks',
                                        state: {
                                            idDrinks: drinks.id,
                                        }
                                    }}><Button className="form-control" color="danger">Detail</Button></Link>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                    <Row>
                        <Col>
                            <div className="pagination-custom">
                                <ul id="page-numbers">
                                    {
                                        pageNumbers.map(number => {
                                            if (this.state.currentPage === number) {
                                                return (
                                                    <li key={number} id={number} className="active">
                                                        {number}
                                                    </li>
                                                )
                                            } else {
                                                return (
                                                    <li key={number} id={number} onClick={this.chosePage}>
                                                        {number}
                                                    </li>
                                                )
                                            }
                                        })
                                    }
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state,
})

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({
            ...actionCreators
        },
        dispatch
    );
}

const connected = connect(mapStateToProps, mapDispatchToProps)(FetchDrinks)

export default connected
